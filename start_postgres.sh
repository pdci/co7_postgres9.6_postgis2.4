#!/bin/bash

DB_NAME=${POSTGRES_DB:-}
DB_USER=${POSTGRES_USER:-}
DB_PASS=${POSTGRES_PASSWORD:-}
PG_CONFDIR="/var/lib/pgsql/data"

__create_user() {
  #Grant rights
  usermod -G wheel postgres

  # Check to see if we have pre-defined credentials to use
if [ -n "${DB_USER}" ]; then
  if [ -z "${DB_PASS}" ]; then
    echo ""
    echo "WARNING: "
    echo "No password specified for \"${DB_USER}\". Generating one"
    echo ""
    DB_PASS=$(pwgen -c -n -1 12)
    echo "Password for \"${DB_USER}\" created as: \"${DB_PASS}\""
  fi
    echo "Creating user \"${DB_USER}\"..."
    echo "CREATE ROLE ${DB_USER} with CREATEROLE login superuser PASSWORD '${DB_PASS}';" |
      sudo -u postgres -H postgres --single \
       -c config_file=${PG_CONFDIR}/postgresql.conf -D ${PG_CONFDIR}
  
fi

if [ -n "${DB_NAME}" ]; then
  echo "Creating database \"${DB_NAME}\"..."
  echo "CREATE DATABASE ${DB_NAME};" | \
    sudo -u postgres -H postgres --single \
     -c config_file=${PG_CONFDIR}/postgresql.conf -D ${PG_CONFDIR}

  if [ -n "${DB_USER}" ]; then
    echo "Granting access to database \"${DB_NAME}\" for user \"${DB_USER}\"..."
    echo "GRANT ALL PRIVILEGES ON DATABASE ${DB_NAME} to ${DB_USER};" |
      sudo -u postgres -H postgres --single \
      -c config_file=${PG_CONFDIR}/postgresql.conf -D ${PG_CONFDIR}
  fi
fi
}


__run_supervisor() {
supervisord -n
}

__run_import_database() {
#psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'db_dev_cotec'";
#
#psql -U postgres -c "DROP DATABASE IF EXISTS db_dev_cotec" ;
#
#psql -U postgres -c "CREATE DATABASE db_dev_cotec  WITH OWNER = postgres; ";
#
#
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS fuzzystrmatch;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_tiger_geocoder;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_topology;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS pg_trgm;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS tablefunc;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_tiger_geocoder;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_topology;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS pg_trgm;";
#psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS tablefunc;";
#
# psql -U postgres -d db_dev_cotec -f /usr/pgsql-9.6/share/contrib/postgis-2.3/legacy.sql

}

# Call all functions
su -l postgres -c "/usr/pgsql-9.6/bin/pg_ctl -D /var/lib/pgsql/9.6/data/ -l logfile start";

__create_user
__run_supervisor
__run_import_database

exec "$@"

