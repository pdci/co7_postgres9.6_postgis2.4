# "ported" by Adam Miller <maxamillion@fedoraproject.org> from
#   https://github.com/fedora-cloud/Fedora-Dockerfiles
#
# Originally written for Fedora-Dockerfiles by
#   scollier <scollier@redhat.com>
#centos:centos7
FROM centos:latest
MAINTAINER The CentOS Project <cloud-ops@centos.org>

RUN yum -y update && \
 yum -y install epel-release && \
# yum -y install sudo \
 echo "exclude=postgresql*" >> /etc/yum.repos.d/CentOS-Base.repo && \
 yum --setopt=tsflags=nodocs -y install systemd && \
 yum --setopt=tsflags=nodocs install -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm && \
 yum --setopt=tsflags=nodocs install -y postgresql96-server postgresql96-contrib postgresql96-devel postgresql96-docs postgresql96-libs perl-DBD-Pg sudo supervisor pwgen && \
 yum clean all

ADD /postgresql-setup /usr/bin/postgresql-setup
ADD /supervisord.conf /etc/supervisord.conf
ADD /start_postgres.sh /start_postgres.sh

RUN chmod +x /usr/bin/postgresql-setup  && \
 chmod +x /start_postgres.sh

#Sudo requires a tty. fix that. (sera que e necessario?????)
#sed -i 's/.*requiretty$/#Defaults requiretty/' /etc/sudoers


WORKDIR /var/lib/pgsql/
#RUN pwd
RUN ln -s  /var/lib/pgsql/9.6/data/ && \
#RUN ls -la /var/lib/pgsql/ && \
#RUN ls -la /var/lib/pgsql/9.6/ && \
 chown postgres:postgres -R /var/lib/pgsql/ && \
#RUN /usr/pgsql-9.6/bin/initdb
 su -l postgres -c "/usr/pgsql-9.6/bin/initdb --pgdata='/var/lib/pgsql/9.6/data/' --auth='ident'" >> /var/lib/pgsql/initdb.log 2>&1 < /dev/null && \
 sed -i -e 's/local   all/#local   all/g' /var/lib/pgsql/9.6/data/pg_hba.conf && \
 sed -i -e 's/host    all/#host    all/g' /var/lib/pgsql/9.6/data/pg_hba.conf && \
 sed -i -e 's/md5/trust/g' /var/lib/pgsql/9.6/data/pg_hba.conf && \
 echo "host    all             all             0.0.0.0/0               trust" >> /var/lib/pgsql/data/pg_hba.conf && \
 echo "local   all             all                                     trust" >> /var/lib/pgsql/data/pg_hba.conf && \
 su -l postgres -c "/usr/pgsql-9.6/bin/pg_ctl -D /var/lib/pgsql/9.6/data/ -l logfile start" >> /var/lib/pgsql/pg_ctl.log 2>&1 < /dev/null
#RUN ls -la /var/lib/pgsql/9.6/data/

ADD /postgresql.conf /var/lib/pgsql/data/postgresql.conf

RUN chown -v postgres.postgres /var/lib/pgsql/data/postgresql.conf

WORKDIR /root/

#instalacao do install_proj4.sh
RUN yum install -y  bzip2 tar zip unzip wget gcc gcc-c++ gcc-g77 flex bison autoconf automake bzip2-devel zlib-devel ncurses-devel libjpeg-devel libpng-devel libtiff-devel freetype-devel pam-devel openssl-devel libxml2-devel gettext-devel pcre-devel && \
#RUN yum grouplist
 yum -y  group mark install "Development Tools"  && \
 yum groupinstall -y "Development tools" "Ferramentas de Desenvolvimento" && \
 wget http://download.osgeo.org/proj/proj-4.9.3.tar.gz && \
 mkdir gdal && \
 tar -zxf proj-4.9.3.tar.gz -C gdal/ >> /root/proj-tar-zxf.log 2>&1 < /dev/null

WORKDIR /root/gdal/proj-4.9.3/
RUN ./configure  && \
 make && \
 make install

WORKDIR /root/
#instalcao do gdal

#libkml Support
##---------This part is depending on prebuilt libraries. In the future we may replace this by compiling them from source.
RUN wget http://s3.amazonaws.com/etc-data.koordinates.com/gdal-travisci/install-libkml-r864-64bit.tar.gz && \
  tar xzf install-libkml-r864-64bit.tar.gz && \
#Copy these required files to  /usr/local
 cp -r install-libkml/include/* /usr/local/include && \
 cp -r install-libkml/lib/* /usr/local/lib && \
 ldconfig && \
#download GDAL
 wget http://download.osgeo.org/gdal/2.2.3/gdal-2.2.3.tar.gz && \
 tar -zxf gdal-2.2.3.tar.gz  >> /root/gdal-tar-zxf.log 2>&1 < /dev/null

WORKDIR /root/gdal-2.2.3/

#Compile from source
RUN ./configure --with-libkml && \
 make && \
 make install

#instalacao do geos

WORKDIR /root/

RUN wget http://download.osgeo.org/geos/geos-3.5.1.tar.bz2 && \
 tar -xjf geos-3.5.1.tar.bz2  >> /root/geos-tar-xjf.log 2>&1 < /dev/null
#RUN yum -y install bzip2

WORKDIR /root/geos-3.5.1/

#RUN cd geos-3.5.1
RUN ./configure && \
 make && \
 make install

#remover diretoria de instalacao dos geos gdal jproj
WORKDIR /root/
RUN rm -f geos-3.5.1.tar.bz2 && \
 rm -f gdal-2.2.3.tar.gz && \
 rm -f proj-4.9.3.tar.gz && \
 rm -rf geos-3.5.1 && \
 rm -rf gdal-2.2.3/ && \
 rm -rf gdal/ && \
 rm -f gdal-tar-zxf.log && \
 rm -f proj-tar-zxf.log && \
 rm -f geos-tar-xjf.log

#instalacao do postgis

RUN yum  --setopt=tsflags=nodocs  -y install postgis23_96-devel && \
 yum clean all

USER postgres
WORKDIR /var/lib/pgsql/
RUN pwd
RUN /usr/pgsql-9.6/bin/pg_ctl -D /var/lib/pgsql/9.6/data/ -l logfile start


RUN psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'db_dev_cotec';" && \
    psql -U postgres -c "DROP DATABASE IF EXISTS db_dev_cotec;" && \
    psql -U postgres -c "CREATE DATABASE db_dev_cotec  WITH OWNER = postgres;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS fuzzystrmatch;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_tiger_geocoder;"  && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_topology;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS pg_trgm;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS tablefunc;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_tiger_geocoder;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS postgis_topology;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS pg_trgm;" && \
    psql -U postgres -d db_dev_cotec -c "create extension IF NOT EXISTS tablefunc;" && \
    psql -U postgres -d db_dev_cotec -f /usr/pgsql-9.6/share/contrib/postgis-2.3/legacy.sql

USER root
#VOLUME ["/var/lib/pgsql"]

EXPOSE 5432
#RUN su -l postgres -c "/usr/pgsql-9.6/bin/pg_ctl -D /var/lib/pgsql/9.6/data/ -l logfile start" >> /var/lib/pgsql/pg_ctl.log 2>&1 < /dev/null
RUN yum clean all && \
 rm -rf /var/cache/yum
#CMD ["/bin/bash", "/start_postgres.sh"]

ENTRYPOINT ["/start_postgres.sh"]

